# Utilities

### There are a no. of files in this directory that have been used for data extraction purposes

- **extract_and_create_dataset**: Creates mini error clips for training (Legacy code for reference purposes only)

- **extract**: First script that I made (Legacy code for reference purposes only)

- **create_dataset_without_dataloader**: Works well but is unnecessary with the existing format of the data loader being used in pytorch (Legacy code for reference purposes only)

- **frame_extract_segmentation**: Extract frame data for different instruments and put them into folders for analysts. This script is a bit buggy but provides a very good foundatio n in case this functionality is needed in the future.

- **blob_analysis**: Extracting key-point information from blobs by using moments

- **database_add_utility**: If there is a need to continue or correct work done using the SegMe application, data needs to be added back to the mongoDB database. That can be done through this script.

- **ext_int_data_accumulator**: Collect data from multiple folders containing both external and internal views.

- **feature_extractor**: Extracts features from the file kp_X.pkl which is created during inference (check inference folder) and converts it into a form that can be used to train a LSTM classifier.

- **feature_vector_cleaner**: Remove NaN from the extracted feature data.

- **generate_cumulative_dataset**: Extracts data from a number of folders each consisting of frames where different tools are present.

- **make_labels_csv_file***:Extract file data and write it to a csv file.

- **osats_extract**: One of the most important scripts in this folder. It takes as input all of the videos that are required for analysis and the csv file with all of the OSATS data. This script then proceeds to break the orginal video down into clips and then frames for all of the data that has an OSATS score associated with it.

- **paper_image_cleaner**: A sandbox script that was used to clean some of the images that were to be put into the paper.

- **segment_extract**: Prepares all of the data in a format compatible with mask-rcnn. Input: Data from SegMe (backup folder); Output: usable_data.pkl

- **video_maker***: Takes in frames and creates videos for demos

- **view_images_in_large_folders**: Viewing frames in certain folders that grow to be too large can sometimes be too difficult to open up. This script attempts to get around that.
