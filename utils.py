def full_path(prefix,case):
    return prefix+case+'.csv'

def file_check(path):
    '''
    purpose:
        - Check to see if a file exists in a specified location
    
    input:
        - folder path
    
    output:
        - True (if the file exists) 
        - False (if the file doesn't exist) 
    '''
    import os
    return os.path.isfile(path)

def dir_check(path):
    '''
    purpose:
        - Check to see if a directory exists in a specified location
    
    input:
        - folder path
    
    output:
        - True (if the directory exists) 
        - False (if the directory doesn't exist) 
    '''
    
    import os
    return os.path.isdir(path)

def create_dir(path):
    '''
    purpose:
        - Create a directory after checking to see if it already exists
    
    input:
        - folder path
    
    output:
        - None
    '''
    
    import os
    # Create the final destination directory
    if (not os.path.isdir(path)):
        os.makedirs(path)
    return "Done"

def agg_data(folder_name,start_time,end_time,data_osats,key,tool_exist,extview_exist):

    data = str(folder_name)+' , ' \
                        +str(tool_exist)+' , ' \
                        +str(extview_exist)+' , ' \
                        +str(start_time)+' , ' \
                        +str(end_time)+' , ' \
                        +str(data_osats['Respect for tissue'][key])+' , ' \
                        +str(data_osats['Time and Motion'][key])+' , ' \
                        +str(data_osats['Instrument handling'][key])+' , ' \
                        +str(data_osats['Knowledge of instruments'][key])+' , ' \
                        +str(data_osats['Use of assistance'][key])+' , ' \
                        +str(data_osats['Flow of operation and forward planning'][key])+' , ' \
                        +str(data_osats['Knowledge of specific procedure'][key])

    return data

def agg_data_ext(folder_name,start_time,end_time):

    data = str(folder_name)+' , ' \
                        +str('False')+' , ' \
                        +str('True')+' , ' \
                        +str(start_time)+' , ' \
                        +str(end_time)+' , ' \
                        +str(0)+' , ' \
                        +str(0)+' , ' \
                        +str(0)+' , ' \
                        +str(0)+' , ' \
                        +str(0)+' , ' \
                        +str(0)+' , ' \
                        +str(0)

    return data

def agg_data_framelevel(file_name,start_time,end_time,tool,ext):

    data = str(file_name)+' , ' \
                        +str(tool)+' , ' \
                        +str(ext)+' , ' \
                        +str(start_time)+' , ' \
                        +str(end_time)+' , ' \
                        +str(0)+' , ' \
                        +str(0)+' , ' \
                        +str(0)+' , ' \
                        +str(0)+' , ' \
                        +str(0)+' , ' \
                        +str(0)+' , ' \
                        +str(0)

    return data

def agg_data_per4(folder_name,start_time,end_time,data_osats,key,tool_exist,extview_exist):

    data = str(folder_name)+' , ' \
                        +str(tool_exist)+' , ' \
                        +str(extview_exist)+' , ' \
                        +str(start_time)+' , ' \
                        +str(end_time)+' , ' \
                        +str(data_osats[key[4]])+' , ' \
                        +str(data_osats[key[5]])+' , ' \
                        +str(data_osats[key[1]])+' , ' \
                        +str(data_osats[key[2]])+' , ' \
                        +str(data_osats[key[6]])+' , ' \
                        +str(data_osats[key[0]])+' , ' \
                        +str(data_osats[key[3]])

    return data

def extract_clip(input_video,init,fini,new_file):
    '''
    purpose:
        - Extract a clip from a video
    
    input:
        - input_video - Input video
        - init - Clip start time (s)
        - fini - Clip end time (s)
        - new_file - Name of the created clip
    
    output:
        - None
    '''
    
    from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip

    ffmpeg_extract_subclip(input_video,init,fini,new_file)
    return "Done"

def write_frames(path,new_file,i,j,case):
    import cv2
    success = True
    count = 0
    vidcap = cv2.VideoCapture(new_file)
    success,image = vidcap.read()
    while success:
        filename = path+case+'_'+str(i)+'_'+str(j)+"_"+"frame"+str(count).zfill(6)+".jpg"
        cv2.imwrite(filename, image) # save frame as JPEG file      
        success,image = vidcap.read()
        count += 1
    return "Done"

def write_frames_2(path,new_file,case):
    import cv2
    import numpy as np
    success = True
    count = 0
    vidcap = cv2.VideoCapture(new_file)
    success,image = vidcap.read()
    while success:
        # True
        filename = path+case+"_"+"frame"+str(count).zfill(6)+".jpg"
        cv2.imwrite(filename, image) # save frame as JPEG file   
        
        # Flip - Vertically
        filename_flipv = path+case+"_"+"frame"+str(count).zfill(6)+"_flipv.jpg"
        cv2.imwrite(filename_flipv, np.flip(image, axis=0)) # save frame as JPEG file   
        
        # Flip - Horizontally
        filename_fliph = path+case+"_"+"frame"+str(count).zfill(6)+"_fliph.jpg"
        cv2.imwrite(filename_fliph, np.flip(image, axis=1)) # save frame as JPEG file           
        success,image = vidcap.read()
        count += 1
#         if (count==2):
#             break;
    return "Done"

def write_frames_3(path,tool,new_file,case,MAX_FRAMES,j,SKIP):
    import cv2
    success = True
    count = 0
    vidcap = cv2.VideoCapture(new_file)
    success,image = vidcap.read()
    while success:
        if (count%SKIP==0):
            filename = path+"frame"+str(count).zfill(6)+"_"+tool+"_"+str(j)+".jpg"
            cv2.imwrite(filename, image) # save frame as JPEG file      
        success,image = vidcap.read()
        count += 1
        if (count==MAX_FRAMES):
            break;
    return "Done"

def write_frames_ext_framelevel(path,new_file,case,output_folder,file_name_labels,tool,ext):
    import cv2
    success = True
    count = 0
    vidcap = cv2.VideoCapture(new_file)
    success,image = vidcap.read()
    while success:
        filename = path+case+"_"+"frame"+str(count).zfill(6)+".jpg"
        cv2.imwrite(filename, image) # save frame as JPEG file      
        success,image = vidcap.read()
        # Write the labels to a file (Can also be done later in the process)
        datum = agg_data_framelevel(filename,0,0,tool,ext)
        write_to_file(file_name_labels,datum)
        count += 1
        if (count == 1000):
            return "Done"
    return "Done"

def write_frames_per4(path,new_file,case):
    import cv2
    success = True
    count = 0
    vidcap = cv2.VideoCapture(new_file)
    success,image = vidcap.read()
    while success:
        filename = path+str(case)+"_"+"frame"+str(count).zfill(6)+".jpg"
        cv2.imwrite(filename, image) # save frame as JPEG file      
        success,image = vidcap.read()
        count += 1
        
#         #debug &&&&&&&&&&&&&&
#         if (count==10000):
#             break;
#         #debug &&&&&&&&&&&&&&
        
    return "Done"

def write_frames_per4_skip(path,new_file,case,SKIP):
    import cv2
    success = True
    count = 0
    vidcap = cv2.VideoCapture(new_file)
    success,image = vidcap.read()
    while success:
        if (count%SKIP==0):
            filename = path+str(case)+"_"+"frame"+str(count).zfill(6)+".jpg"
            cv2.imwrite(filename, image) # save frame as JPEG file      
        success,image = vidcap.read()
        count += 1
            
    return "Done"

def write_frames_start_skip_end(path,new_file,case,START,SKIP,END):
    '''
    purpose:
        - Extract frames from a clip
    
    input:
        - path - Folder path to write the frames
        - new_file - Full path of the video to be extracted
        - case - Case name (appended to the name of the files)
        - SKIP - Name of frames to skip
        - END - No. frames to extract before stopping
    
    output:
        - None
    '''
    import cv2
    success = True
    count = 0
    vidcap = cv2.VideoCapture(new_file)
    success,image = vidcap.read()
    while success:
        if (count>START):
            if (count%SKIP==0):
                filename = path+str(case)+"_"+"frame"+str(count).zfill(6)+".jpg"
                cv2.imwrite(filename, image) # save frame as JPEG file
        success,image = vidcap.read()
        count += 1
        if (count>END):
            break;
            
    return "Done"

def write_frames_colorization(path,new_file,case):
    import cv2
    success = True
    count = 0
    vidcap = cv2.VideoCapture(new_file)
    success,image = vidcap.read()
    while success:
        # write a frame every X frames
        if (count%30==0):
            filename = path+str(case)+"_"+"frame"+str(count).zfill(6)+".jpg"
            cv2.imwrite(filename, image) # save frame as JPEG file      
        success,image = vidcap.read()
        count += 1
        
    return "Done"

def write_frames_per4_filename(path,new_file,case):
    import cv2
    success = True
    count = 0
    vidcap = cv2.VideoCapture(new_file)
    success,image = vidcap.read()
    filenames = []
    while success:
        filename = path+case+"_"+"frame"+str(count).zfill(6)+".jpg"
        filenames.append(filename) # Capture all of the filenames
        cv2.imwrite(filename, image) # save frame as JPEG file      
        success,image = vidcap.read()
        count += 1
    return filenames

def write_to_file(LABELS_FILE,data):
    '''
    purpose:
        - Write data to a file (line-by-line)
    
    input:
        - LABELS_FILE - File that the data will be written to
        - data - Data to be written
   
    output:
        - None
    '''
        
    import os
    if (os.path.isfile(LABELS_FILE)):
        f = open(LABELS_FILE, 'a')
        f.write(datum+'\n')
        f.close()
    else:
        print(LABELS_FILE+' does not exist!')
    return "Done"

def data_split(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df):
    import os
    from subprocess import call
    # Calculate the required indices
    indx_trn = int(train_per*len(cases))
    indx_val = int((train_per+valid_per)*len(cases))

    # Break cases down into training, validation and test sets
    cases_train = cases[:indx_trn]
    cases_valid = cases[indx_trn+1:indx_val]
    cases_test = cases[indx_val+1:]
    DELETE_PATH = './../csv-data/OSATS_per4/'

    # Print some case statistics
    print('Training cases: '+str(len(cases_train)))
    print(cases_train)
    print('Validation cases: '+str(len(cases_valid)))
    print(cases_valid)
    print('Testing cases: '+str(len(cases_test)))
    print(cases_test)

    # Extract data from OSATS file and write it to different files for the data-loader
    # ex: [labels_train.txt, labels_valid.txt, labels_test.txt]

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"labels_train.txt"])
    call(["rm", DELETE_PATH+"labels_valid.txt"])
    call(["rm", DELETE_PATH+"labels_test.txt"])

    for case in cases:
        # Run the query
        result = df[(df['folder']).str.contains(case)]
        result = result.to_csv(header=False, index=False)

    #     if (result.shape[0]>0):
        # Open file and append to it
        if case in cases_train:
            # ---Train
            if (os.path.isfile(LABELS_FILE_TRAIN)):
                f = open(LABELS_FILE_TRAIN, 'a')
                f.write(result)
                f.close()
            else:
                #print(LABELS_FILE_TRAIN+' does not exist!')
                f = open(LABELS_FILE_TRAIN, 'w')
                f.write(result)
                f.close()

        elif case in cases_valid:
            # ---Valid
            if (os.path.isfile(LABELS_FILE_VALID)):
                f = open(LABELS_FILE_VALID, 'a')
                f.write(result)
                f.close()
            else:
                #print(LABELS_FILE_VALID+' does not exist!')
                f = open(LABELS_FILE_VALID, 'w')
                f.write(result)
                f.close()

        elif case in cases_test:
            # ---Test
            if (os.path.isfile(LABELS_FILE_TEST)):
                f = open(LABELS_FILE_TEST, 'a')
                f.write(result)
                f.close()
            else:
                #print(LABELS_FILE_TEST+' does not exist!')
                f = open(LABELS_FILE_TEST, 'w')
                f.write(result)
                f.close()

    return "Files Created!"

def data_split_ext(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df,df_ext):
    import os
    from subprocess import call

    # Calculate the required indices 
    indx_trn = int(train_per*len(cases))
    indx_val = int((train_per+valid_per)*len(cases))

    # Break cases down into training, validation and test sets
    cases_train = cases[:indx_trn]
    cases_valid = cases[indx_trn+1:indx_val]
    cases_test = cases[indx_val+1:]
    DELETE_PATH = './../csv-data/'

    # Print some case statistics
    print('Training cases: '+str(len(cases_train)))
    print(cases_train)
    print('Validation cases: '+str(len(cases_valid)))
    print(cases_valid)
    print('Testing cases: '+str(len(cases_test)))
    print(cases_test)


    # Extract data from OSATS file and write it to different files for the data-loader
    # ex: [labels_train_ext.txt, labels_valid_ext.txt, labels_test_ext.txt]

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"labels_ext_train.txt"])
    call(["rm", DELETE_PATH+"labels_ext_valid.txt"])
    call(["rm", DELETE_PATH+"labels_ext_test.txt"])

    for case in cases:
        # Run the query
        result_ext = df_ext[(df_ext['filename']).str.contains(case)]
        result_ext = result_ext.to_csv(header=False, index=False)
        result = df[(df['filename']).str.contains(case)]
        result = result[:15].to_csv(header=False, index=False)

    #     if (result.shape[0]>0):
        # Open file and append to it
        if case in cases_train:
            # ---Train
            if (os.path.isfile(LABELS_FILE_TRAIN)):
                f = open(LABELS_FILE_TRAIN, 'a')
                f.write(result_ext)				
                f.write(result)
                f.close()
            else:
                #print(LABELS_FILE_TRAIN+' does not exist!')
                f = open(LABELS_FILE_TRAIN, 'w')
                f.write(result_ext)				
                f.write(result)
                f.close()

        elif case in cases_valid:
            # ---Valid
            if (os.path.isfile(LABELS_FILE_VALID)):
                f = open(LABELS_FILE_VALID, 'a')
                f.write(result_ext)				
                f.write(result)
                f.close()
            else:
                #print(LABELS_FILE_VALID+' does not exist!')
                f = open(LABELS_FILE_VALID, 'w')
                f.write(result_ext)				
                f.write(result)
                f.close()

        elif case in cases_test:
            # ---Test
            if (os.path.isfile(LABELS_FILE_TEST)):
                f = open(LABELS_FILE_TEST, 'a')
                f.write(result_ext)				
                f.write(result)
                f.close()
            else:
                #print(LABELS_FILE_TEST+' does not exist!')
                f = open(LABELS_FILE_TEST, 'w')
                f.write(result_ext)				
                f.write(result)
                f.close()

    return "Files Created!"


def data_split_ext_frame(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df_ext):
    import os
    from subprocess import call

    # Calculate the required indices 
    indx_trn = int(train_per*len(cases))
    indx_val = int((train_per+valid_per)*len(cases))

    # Break cases down into training, validation and test sets
    cases_train = cases[:indx_trn]
    cases_valid = cases[indx_trn+1:indx_val]
    cases_test = cases[indx_val+1:]
    DELETE_PATH = './../csv-data/'

    # Print some case statistics
    print('Training cases: '+str(len(cases_train)))
    print(cases_train)
    print('Validation cases: '+str(len(cases_valid)))
    print(cases_valid)
    print('Testing cases: '+str(len(cases_test)))
    print(cases_test)


    # Extract data from OSATS file and write it to different files for the data-loader
    # ex: [labels_train_ext.txt, labels_valid_ext.txt, labels_test_ext.txt]

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"labels_framelevel_train.txt"])
    call(["rm", DELETE_PATH+"labels_framelevel_valid.txt"])
    call(["rm", DELETE_PATH+"labels_framelevel_test.txt"])

    for case in cases:
        # Run the query
        result_ext = df_ext[(df_ext['filename']).str.contains(case)]
        result_ext = result_ext.to_csv(header=False, index=False)
    #     if (result.shape[0]>0):
        # Open file and append to it
        if case in cases_train:
            # ---Train
            if (os.path.isfile(LABELS_FILE_TRAIN)):
                f = open(LABELS_FILE_TRAIN, 'a')
                f.write(result_ext)
                f.close()
            else:
                #print(LABELS_FILE_TRAIN+' does not exist!')
                f = open(LABELS_FILE_TRAIN, 'w')
                f.write(result_ext)
                f.close()

        elif case in cases_valid:
            # ---Valid
            if (os.path.isfile(LABELS_FILE_VALID)):
                f = open(LABELS_FILE_VALID, 'a')
                f.write(result_ext)
                f.close()
            else:
                #print(LABELS_FILE_VALID+' does not exist!')
                f = open(LABELS_FILE_VALID, 'w')
                f.write(result_ext)
                f.close()

        elif case in cases_test:
            # ---Test
            if (os.path.isfile(LABELS_FILE_TEST)):
                f = open(LABELS_FILE_TEST, 'a')
                f.write(result_ext)
                f.close()
            else:
                #print(LABELS_FILE_TEST+' does not exist!')
                f = open(LABELS_FILE_TEST, 'w')
                f.write(result_ext)
                f.close()

    return "Files Created!"

def data_split_ext_frame_2(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df_ext,data):
    import os
    from subprocess import call
    import numpy as np

    # Calculate the required indices 
    indx_trn = int(train_per*len(cases))
    indx_val = int((train_per+valid_per)*len(cases))
    
    # Sort the cases
    #cases.sort(axis=0)
    
    # Break cases down into training, validation and test sets
    cases_train = np.asarray(cases[:indx_trn])
    cases_valid = np.asarray(cases[indx_trn+1:indx_val])
    cases_test = np.asarray(cases[indx_val+1:])
    DELETE_PATH = './'

    # Assuming evenly sized cases, extract the required data 
    # for each of the testcases
    data_train = []
    data_valid = []
    data_test = []

    #print('df_ext[\'frame_name\']: {}'.format(df_ext['frame_name']))
    
    for case in cases_train:
        [data_train.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_train)
    print('data_train extraction complete')
    for case in cases_valid:
        [data_valid.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_valid)
    print('data_valid extraction complete')
    for case in cases_test:
        [data_test.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_test)
    print('data_test extraction complete')
    
    # Print some case statistics
    print('Training cases: '+str(len(data_train)))
    #print(data_train)
    print('Validation cases: '+str(len(data_valid)))
    #print(data_valid)
    print('Testing cases: '+str(len(data_test)))
    #print(data_test)

    # Extract data from OSATS file and write it to different files for the data-loader
    # ex: [labels_train_ext.txt, labels_valid_ext.txt, labels_test_ext.txt]

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"OSATS_train.txt"])
    call(["rm", DELETE_PATH+"OSATS_valid.txt"])
    call(["rm", DELETE_PATH+"OSATS_test.txt"])
    
    # Sort the files to allow for sorted training
#     cases_train.sort(axis=0) 
#     cases_valid.sort(axis=0)
#     cases_test.sort(axis=0)     
    
    print()
    print('Saving files...')
    print()
    # ---Train
    np.savetxt(LABELS_FILE_TRAIN, data_train, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_VALID, data_valid, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_TEST, data_test, delimiter=",", fmt='%s')

#     # ---Valid
#     if (os.path.isfile(LABELS_FILE_VALID)):
#         f = open(LABELS_FILE_VALID, 'a')
#         f.write(result_valid)
#         f.close()
#     else:
#         #print(LABELS_FILE_VALID+' does not exist!')
#         f = open(LABELS_FILE_VALID, 'w')
#         f.write(result_valid)
#         f.close()

#     # ---Test
#     if (os.path.isfile(LABELS_FILE_TEST)):
#         f = open(LABELS_FILE_TEST, 'a')
#         f.write(result_test)
#         f.close()
#     else:
#         #print(LABELS_FILE_TEST+' does not exist!')
#         f = open(LABELS_FILE_TEST, 'w')
#         f.write(result_test)
#         f.close()

    return "Files Created!"


def data_split_x_val_LOUO(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df_ext,data,config,user):
    import os
    from subprocess import call
    import numpy as np

    # Calculate the required indices 
    reg_exp = '_'+str(user)+'00'
    cases = np.array(cases)

    # Break cases down into training, validation and test sets
    cases_train = np.array([v for v in cases if reg_exp not in v])
    cases_valid = np.array([v for v in cases if reg_exp in v])
    #cases_test = np.asarray(cases[indx_tst])
    DELETE_PATH = './'

    # Assuming evenly sized cases, extract the required data 
    # for each of the testcases
    data_train = []
    data_valid = []
    data_test = []
    
    for case in cases_train:
        [data_train.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_train)
    print('data_train extraction complete')
    for case in cases_valid:
        [data_valid.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_valid)
    print('data_valid extraction complete')
    # for case in cases_test:
    #     [data_test.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
    #     np.random.shuffle(data_test)
    # print('data_test extraction complete')
    
    # Print some case statistics
    print('Training cases: '+str(len(data_train)))
    #print(data_train)
    print('Validation cases: '+str(len(data_valid)))
    #print(data_valid)
    # print('Testing cases: '+str(len(data_test)))
    #print(data_test)

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"OSATS_train.txt"])
    call(["rm", DELETE_PATH+"OSATS_valid.txt"])
    # call(["rm", DELETE_PATH+"OSATS_test.txt"])
    
    # ---Train
    np.savetxt(LABELS_FILE_TRAIN, data_train, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_VALID, data_valid, delimiter=",", fmt='%s')
    # np.savetxt(LABELS_FILE_TEST, data_test, delimiter=",", fmt='%s')

    return "Files Created!"

def data_split_x_val_LOSO(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df_ext,data,config,super_trial):
    import os
    from subprocess import call
    import numpy as np

    # Calculate the required indices 
    reg_exp = '00'+str(super_trial)
    cases = np.array(cases)

    # Break cases down into training, validation and test sets
    cases_train = np.array([v for v in cases if reg_exp not in v])
    cases_valid = np.array([v for v in cases if reg_exp in v])
    #cases_test = np.asarray(cases[indx_tst])
    DELETE_PATH = './'

    # Assuming evenly sized cases, extract the required data 
    # for each of the testcases
    data_train = []
    data_valid = []
    data_test = []
    
    for case in cases_train:
        [data_train.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_train)
    print('data_train extraction complete')
    for case in cases_valid:
        [data_valid.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_valid)
    print('data_valid extraction complete')
    # for case in cases_test:
    #     [data_test.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
    #     np.random.shuffle(data_test)
    # print('data_test extraction complete')
    
    # Print some case statistics
    print('Training cases: '+str(len(data_train)))
    #print(data_train)
    print('Validation cases: '+str(len(data_valid)))
    #print(data_valid)
    # print('Testing cases: '+str(len(data_test)))
    #print(data_test)

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"OSATS_train.txt"])
    call(["rm", DELETE_PATH+"OSATS_valid.txt"])
    # call(["rm", DELETE_PATH+"OSATS_test.txt"])
    
    # ---Train
    np.savetxt(LABELS_FILE_TRAIN, data_train, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_VALID, data_valid, delimiter=",", fmt='%s')
    # np.savetxt(LABELS_FILE_TEST, data_test, delimiter=",", fmt='%s')

    return "Files Created!"


def data_split_ext_frame_aug(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df_ext,data):
    import os
    from subprocess import call
    import numpy as np

    # Calculate the required indices 
    indx_trn = int(train_per*len(cases))
    indx_val = int((train_per+valid_per)*len(cases))
    
    # Sort the cases
    #cases.sort(axis=0)
    
    # Break cases down into training, validation and test sets
    cases_train = np.asarray(cases[:indx_trn])
    cases_valid = np.asarray(cases[indx_trn+1:indx_val])
    cases_test = np.asarray(cases[indx_val+1:])
    DELETE_PATH = './'

    # Assuming evenly sized cases, extract the required data 
    # for each of the testcases
    data_train = []
    data_valid = []
    data_test = []

    print('df_ext[\'frame_name\']: {}'.format(df_ext['frame_name']))
    
    for case in cases_train:
        #print(case)
        [data_train.append(case)]
        np.random.shuffle(data_train)
    print('data_train extraction complete')
    for case in cases_valid:
        [data_valid.append(case)]
        np.random.shuffle(data_valid)
    print('data_valid extraction complete')
    for case in cases_test:
        [data_test.append(case)]
        np.random.shuffle(data_test)
    print('data_test extraction complete')
    
    # Print some case statistics
    print('Training cases: '+str(len(data_train)))
    #print(data_train)
    print('Validation cases: '+str(len(data_valid)))
    #print(data_valid)
    print('Testing cases: '+str(len(data_test)))
    #print(data_test)

    # Extract data from OSATS file and write it to different files for the data-loader
    # ex: [labels_train_ext.txt, labels_valid_ext.txt, labels_test_ext.txt]

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"OSATS_train.txt"])
    call(["rm", DELETE_PATH+"OSATS_valid.txt"])
    call(["rm", DELETE_PATH+"OSATS_test.txt"]) 

    # ---Train
    np.savetxt(LABELS_FILE_TRAIN, data_train, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_VALID, data_valid, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_TEST, data_test, delimiter=",", fmt='%s')

    return "Files Created!"

def data_split_x_val(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df_ext,data,config):
    import os
    from subprocess import call
    import numpy as np

    # Calculate the required indices 
    indx_trn = int(train_per*len(cases))
    indx_val = int((train_per+valid_per)*len(cases))
    
    # Sort the cases
    #cases.sort(axis=0)
    
    # Break cases down into training, validation and test sets
    cases_train = np.asarray(cases[:indx_trn])
    cases_valid = np.asarray(cases[indx_trn+1:indx_val])
    cases_test = np.asarray(cases[indx_val+1:])
    DELETE_PATH = './'

    # Assuming evenly sized cases, extract the required data 
    # for each of the testcases
    data_train = []
    data_valid = []
    data_test = []
    
    for case in cases_train:
        [data_train.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_train)
    print('data_train extraction complete')
    for case in cases_valid:
        [data_valid.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_valid)
    print('data_valid extraction complete')
    for case in cases_test:
        [data_test.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_test)
    print('data_test extraction complete')
    
    # Print some case statistics
    print('Training cases: '+str(len(data_train)))
    #print(data_train)
    print('Validation cases: '+str(len(data_valid)))
    #print(data_valid)
    print('Testing cases: '+str(len(data_test)))
    #print(data_test)

    # Extract data from OSATS file and write it to different files for the data-loader
    # ex: [labels_train_ext.txt, labels_valid_ext.txt, labels_test_ext.txt]

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"OSATS_train.txt"])
    call(["rm", DELETE_PATH+"OSATS_valid.txt"])
    call(["rm", DELETE_PATH+"OSATS_test.txt"])
    
    # Sort the files to allow for sorted training
#     cases_train.sort(axis=0) 
#     cases_valid.sort(axis=0)
#     cases_test.sort(axis=0)     

    # ---Train
    np.savetxt(LABELS_FILE_TRAIN, data_train, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_VALID, data_valid, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_TEST, data_test, delimiter=",", fmt='%s')

#     # ---Valid
#     if (os.path.isfile(LABELS_FILE_VALID)):
#         f = open(LABELS_FILE_VALID, 'a')
#         f.write(result_valid)
#         f.close()
#     else:
#         #print(LABELS_FILE_VALID+' does not exist!')
#         f = open(LABELS_FILE_VALID, 'w')
#         f.write(result_valid)
#         f.close()

#     # ---Test
#     if (os.path.isfile(LABELS_FILE_TEST)):
#         f = open(LABELS_FILE_TEST, 'a')
#         f.write(result_test)
#         f.close()
#     else:
#         #print(LABELS_FILE_TEST+' does not exist!')
#         f = open(LABELS_FILE_TEST, 'w')
#         f.write(result_test)
#         f.close()

    return "Files Created!"

def data_split_x_val_aug(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df_ext,data,config):
    import os
    from subprocess import call
    import numpy as np

    # Calculate the required indices 
    indx_trn = int(train_per*len(cases))
    indx_val = int((train_per+valid_per)*len(cases))

    # Sort the cases
    #cases.sort(axis=0)
    
    # Break cases down into training, validation and test sets
    cases_train = np.asarray(cases[:indx_trn])
    cases_valid = np.asarray(cases[indx_trn+1:indx_val])
    cases_test = np.asarray(cases[indx_val+1:])
    DELETE_PATH = './'

    print('Training cases: '+str(len(cases_train)))
    #print(data_train)
    print('Validation cases: '+str(len(cases_valid)))
    #print(data_valid)
    print('Testing cases: '+str(len(cases_test)))
    
    # Assuming evenly sized cases, extract the required data 
    # for each of the testcases
    data_train = []
    data_valid = []
    data_test = []
    
    for case in cases_train:
        [data_train.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_train)
    print('data_train extraction complete')
    for case in cases_valid:
        [data_valid.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_valid)
    print('data_valid extraction complete')
    for case in cases_test:
        [data_test.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
        np.random.shuffle(data_test)
    print('data_test extraction complete')
    
    # Print some case statistics
    print('Training cases: '+str(len(data_train)))
    #print(data_train)
    print('Validation cases: '+str(len(data_valid)))
    #print(data_valid)
    print('Testing cases: '+str(len(data_test)))
    #print(data_test)

    # Extract data from OSATS file and write it to different files for the data-loader
    # ex: [labels_train_ext.txt, labels_valid_ext.txt, labels_test_ext.txt]

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"OSATS_train.txt"])
    call(["rm", DELETE_PATH+"OSATS_valid.txt"])
    call(["rm", DELETE_PATH+"OSATS_test.txt"])
    
    # Sort the files to allow for sorted training
#     cases_train.sort(axis=0) 
#     cases_valid.sort(axis=0)
#     cases_test.sort(axis=0)     

    # ---Train
    np.savetxt(LABELS_FILE_TRAIN, data_train, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_VALID, data_valid, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_TEST, data_test, delimiter=",", fmt='%s')

#     # ---Valid
#     if (os.path.isfile(LABELS_FILE_VALID)):
#         f = open(LABELS_FILE_VALID, 'a')
#         f.write(result_valid)
#         f.close()
#     else:
#         #print(LABELS_FILE_VALID+' does not exist!')
#         f = open(LABELS_FILE_VALID, 'w')
#         f.write(result_valid)
#         f.close()

#     # ---Test
#     if (os.path.isfile(LABELS_FILE_TEST)):
#         f = open(LABELS_FILE_TEST, 'a')
#         f.write(result_test)
#         f.close()
#     else:
#         #print(LABELS_FILE_TEST+' does not exist!')
#         f = open(LABELS_FILE_TEST, 'w')
#         f.write(result_test)
#         f.close()

    return "Files Created!"


def data_split_ext_frame_5(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df_ext,data):
    import os
    from subprocess import call
    import numpy as np

    # Calculate the required indices 
    indx_trn = int(train_per*len(cases))
    indx_val = int((train_per+valid_per)*len(cases))
    
    # Sort the cases
    #cases.sort(axis=0)
    
    # Break cases down into training, validation and test sets
    cases_train = np.asarray(cases[:indx_trn])
    cases_valid = np.asarray(cases[indx_trn+1:indx_val])
    cases_test = np.asarray(cases[indx_val+1:])
    DELETE_PATH = './'

    # Assuming evenly sized cases, extract the required data 
    # for each of the testcases
    data_train = []
    data_valid = []
    data_test = []
    
    for case in cases_train:
        [data_train.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
    print('data_train extraction complete')
    for case in cases_valid:
        [data_valid.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
    print('data_valid extraction complete')
    for case in cases_test:
        [data_test.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
    print('data_test extraction complete')
    
    # Print some case statistics
    print('Training cases: '+str(len(data_train)))
    #print(data_train)
    print('Validation cases: '+str(len(data_valid)))
    #print(data_valid)
    print('Testing cases: '+str(len(data_test)))
    #print(data_test)

    # Extract data from OSATS file and write it to different files for the data-loader
    # ex: [labels_train_ext.txt, labels_valid_ext.txt, labels_test_ext.txt]

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"OSATS_train.txt"])
    call(["rm", DELETE_PATH+"OSATS_valid.txt"])
    call(["rm", DELETE_PATH+"OSATS_test.txt"])
    
    # Sort the files to allow for sorted training
#     cases_train.sort(axis=0) 
#     cases_valid.sort(axis=0)
#     cases_test.sort(axis=0)     

    # ---Train
    np.savetxt(LABELS_FILE_TRAIN, data_train, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_VALID, data_valid, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_TEST, data_test, delimiter=",", fmt='%s')

#     # ---Valid
#     if (os.path.isfile(LABELS_FILE_VALID)):
#         f = open(LABELS_FILE_VALID, 'a')
#         f.write(result_valid)
#         f.close()
#     else:
#         #print(LABELS_FILE_VALID+' does not exist!')
#         f = open(LABELS_FILE_VALID, 'w')
#         f.write(result_valid)
#         f.close()

#     # ---Test
#     if (os.path.isfile(LABELS_FILE_TEST)):
#         f = open(LABELS_FILE_TEST, 'a')
#         f.write(result_test)
#         f.close()
#     else:
#         #print(LABELS_FILE_TEST+' does not exist!')
#         f = open(LABELS_FILE_TEST, 'w')
#         f.write(result_test)
#         f.close()

    return "Files Created!"


def data_split_ext_frame_3(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df_ext,data):
    import os
    from subprocess import call
    import numpy as np

    # Calculate the required indices
    indx_trn = int(train_per*df_ext.shape[0])
    indx_val = int((train_per+valid_per)*df_ext.shape[0])
    
    # Sort the cases
    #cases.sort(axis=0)
    
    # Break cases down into training, validation and test sets
#     cases_train = np.asarray(cases[:indx_trn])
#     cases_valid = np.asarray(cases[indx_trn+1:indx_val])
#     cases_test = np.asarray(cases[indx_val+1:])
    DELETE_PATH = './../csv-data/tool/'

    # Assuming evenly sized cases, extract the required data 
    # for each of the testcases
    data_train = []
    data_valid = []
    data_test = []
    
#     for case in cases_train:
#         [data_train.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
#     print('data_train extraction complete')
#     for case in cases_valid:
#         [data_valid.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
#     print('data_valid extraction complete')
#     for case in cases_test:
#         [data_test.append(v) for v in df_ext[df_ext['frame_name'].str.contains(case)].values]
#     print('data_test extraction complete')

    data_train = df_ext[:indx_trn].values
    data_valid = df_ext[indx_trn+1:indx_val].values
    data_test = df_ext[indx_val+1:].values
    
    # Print some case statistics
    print('Training cases: '+str(len(data_train)))
    #print(data_train)
    print('Validation cases: '+str(len(data_valid)))
    #print(data_valid)
    print('Testing cases: '+str(len(data_test)))
    #print(data_test)

    # Extract data from OSATS file and write it to different files for the data-loader
    # ex: [labels_train_ext.txt, labels_valid_ext.txt, labels_test_ext.txt]

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"labels_framelevel_train.txt"])
    call(["rm", DELETE_PATH+"labels_framelevel_valid.txt"])
    call(["rm", DELETE_PATH+"labels_framelevel_test.txt"])
    
    # Sort the files to allow for sorted training
#     cases_train.sort(axis=0) 
#     cases_valid.sort(axis=0)
#     cases_test.sort(axis=0)     
        
    # ---Train
    np.savetxt(LABELS_FILE_TRAIN, data_train, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_VALID, data_valid, delimiter=",", fmt='%s')
    np.savetxt(LABELS_FILE_TEST, data_test, delimiter=",", fmt='%s')

    return "Files Created!"


def data_split_osats_per4(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df_ext):
    import os
    from subprocess import call

    # Calculate the required indices 
    indx_trn = int(train_per*len(cases))
    indx_val = int((train_per+valid_per)*len(cases))

    # Break cases down into training, validation and test sets
    cases_train = cases[:indx_trn]
    cases_valid = cases[indx_trn+1:indx_val]
    cases_test = cases[indx_val+1:]
    DELETE_PATH = './../csv-data/OSATS_per4'

    # Print some case statistics
    print('Training cases: '+str(len(cases_train)))
    print(cases_train)
    print('Validation cases: '+str(len(cases_valid)))
    print(cases_valid)
    print('Testing cases: '+str(len(cases_test)))
    print(cases_test)


    # Extract data from OSATS file and write it to different files for the data-loader
    # ex: [labels_train_ext.txt, labels_valid_ext.txt, labels_test_ext.txt]

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"labels_train.txt"])
    call(["rm", DELETE_PATH+"labels_valid.txt"])
    call(["rm", DELETE_PATH+"labels_test.txt"])

    for case in cases:
        # Run the query
        result_ext = df_ext[(df_ext['filename']).str.contains(case)]
        result_ext = result_ext.to_csv(header=False, index=False)

    #     if (result.shape[0]>0):
        # Open file and append to it
        if case in cases_train:
            # ---Train
            if (os.path.isfile(LABELS_FILE_TRAIN)):
                f = open(LABELS_FILE_TRAIN, 'a')
                f.write(result_ext)
                f.close()
            else:
                #print(LABELS_FILE_TRAIN+' does not exist!')
                f = open(LABELS_FILE_TRAIN, 'w')
                f.write(result_ext)
                f.close()

        elif case in cases_valid:
            # ---Valid
            if (os.path.isfile(LABELS_FILE_VALID)):
                f = open(LABELS_FILE_VALID, 'a')
                f.write(result_ext)
                f.close()
            else:
                #print(LABELS_FILE_VALID+' does not exist!')
                f = open(LABELS_FILE_VALID, 'w')
                f.write(result_ext)
                f.close()

        elif case in cases_test:
            # ---Test
            if (os.path.isfile(LABELS_FILE_TEST)):
                f = open(LABELS_FILE_TEST, 'a')
                f.write(result_ext)				
                f.close()
            else:
                #print(LABELS_FILE_TEST+' does not exist!')
                f = open(LABELS_FILE_TEST, 'w')
                f.write(result_ext)				
                f.close()

    return "Files Created!"

def data_split_ver3(train_per,valid_per,test_per,cases,LABELS_FILE_TRAIN,LABELS_FILE_TEST,LABELS_FILE_VALID,df_ext):
    import os
    from subprocess import call

    # Calculate the required indices 
    indx_trn = int(train_per*len(cases))
    indx_val = int((train_per+valid_per)*len(cases))

    # Break cases down into training, validation and test sets
    cases_train = cases[:indx_trn]
    cases_valid = cases[indx_trn+1:indx_val]
    cases_test = cases[indx_val+1:]
    DELETE_PATH = './../csv-data/OSATS_per4'

    # Print some case statistics
    print('Training cases: '+str(len(cases_train)))
    print(cases_train)
    print('Validation cases: '+str(len(cases_valid)))
    print(cases_valid)
    print('Testing cases: '+str(len(cases_test)))
    print(cases_test)


    # Extract data from OSATS file and write it to different files for the data-loader
    # ex: [labels_train_ext.txt, labels_valid_ext.txt, labels_test_ext.txt]

    # Delete the files if they exist
    call(["rm", DELETE_PATH+"labels_train.txt"])
    call(["rm", DELETE_PATH+"labels_valid.txt"])
    call(["rm", DELETE_PATH+"labels_test.txt"])

    for case in cases:
        # Run the query
        result_ext = df_ext[(df_ext['filename']).str.contains(case)]
        result_ext = result_ext.to_csv(header=False, index=False)

    #     if (result.shape[0]>0):
        # Open file and append to it
        if case in cases_train:
            # ---Train
            if (os.path.isfile(LABELS_FILE_TRAIN)):
                f = open(LABELS_FILE_TRAIN, 'a')
                f.write(result_ext)
                f.close()
            else:
                #print(LABELS_FILE_TRAIN+' does not exist!')
                f = open(LABELS_FILE_TRAIN, 'w')
                f.write(result_ext)
                f.close()

        elif case in cases_valid:
            # ---Valid
            if (os.path.isfile(LABELS_FILE_VALID)):
                f = open(LABELS_FILE_VALID, 'a')
                f.write(result_ext)
                f.close()
            else:
                #print(LABELS_FILE_VALID+' does not exist!')
                f = open(LABELS_FILE_VALID, 'w')
                f.write(result_ext)
                f.close()

        elif case in cases_test:
            # ---Test
            if (os.path.isfile(LABELS_FILE_TEST)):
                f = open(LABELS_FILE_TEST, 'a')
                f.write(result_ext)				
                f.close()
            else:
                #print(LABELS_FILE_TEST+' does not exist!')
                f = open(LABELS_FILE_TEST, 'w')
                f.write(result_ext)				
                f.close()

    return "Files Created!"

def gen_key_points(masked_data):
    import numpy as np
    import cv2
    #pic = imageio.imread(filename)

    masked_data = np.mean(masked_data, axis=2)

    # Find coordinates of all the values that are greater than 0
    coords = np.array(np.where(masked_data>0))
    coords_flag = coords.shape[1]

    if (coords.shape[1]>0):
        # Find the min and max 'x' values and coupled y values
        indx_min = np.where(coords[0,:]==np.min(coords[0,:]))[0][0] # Find the index of the min value
        min_x, min_y_coup = coords[:,indx_min]
        #print(indx_min, min_x, min_y_coup)

        indx_max = np.where(coords[0,:]==np.max(coords[0,:]))[0][-1] # Find the index of the min value
        max_x, max_y_coup = coords[:,indx_max]
        #print(indx_max, max_x, max_y_coup)

        # Find the min and max 'y' values and coupled x values
        indy_min = np.where(coords[1,:]==np.min(coords[1,:]))[0][0] # Find the index of the min value
        min_x_coup, min_y = coords[:,indy_min]
        #print(indy_min, min_x_coup, min_y)

        indy_max = np.where(coords[1,:]==np.max(coords[1,:]))[0][-1] # Find the index of the min value
        max_x_coup, max_y = coords[:,indy_max]
        #print(indy_max, max_x_coup, max_y)

        img_1 = masked_data.copy()
        # img_2 = masked_data.copy()
        # img_kp_1 = cv2.line(img_1, (min_y_coup, min_x), (max_y_coup, max_x), (255,255,255), thickness=13, lineType=8)
        # img_kp_2 = cv2.line(img_kp_1, (max_y, max_x_coup), (min_y, min_x_coup), (127,127,127), thickness=13, lineType=8)
        slope = (max_y_coup-min_y_coup)/(max_x-min_x)

        if (slope>0):
            x0 = int((min_y_coup+min_y)/2)
            y0 = int((min_x_coup+min_x)/2)
            x1 = int((max_y+max_y_coup)/2)
            y1 = int((max_x_coup+max_x)/2)
            img_kp_mean = cv2.line(img_1, (x0, y0), (x1, y1), (255,255,255), thickness=20, lineType=10)
        else:
            x0 = int((min_y_coup+max_y)/2)
            y0 = int((min_x+max_x_coup)/2)
            x1 = int((max_y_coup+min_y)/2)
            y1 = int((max_x+min_x_coup)/2)
            img_kp_mean = cv2.line(img_1, (x0, y0), (x1, y1), (255,255,255), thickness=20, lineType=10)
        return masked_data, x0, y0, x1, y1

    else:
        return 0, 0, 0, 0, 0

def gen_key_points_blob(masked_data):
    import numpy as np
    import cv2
    #pic = imageio.imread(filename)

    masked_data = np.mean(masked_data, axis=2)
    values_reqd = {'cx': 0, 'cy': 0, 'alpha': 0, 'length': 0}

    # Get the coordinates of all of the values that are not zero
    y, x = np.nonzero(masked_data)
    x = x-np.mean(x)
    y = y-np.mean(y)
    coords = np.stack([x, y])
#     print(coords[0].shape)
#     print(len(coords[0]))
#     print(coords.shape[1])
    if (len(coords[0])>1):
        cov = np.cov(coords)
        evals, evecs = np.linalg.eig(cov)

        # Find the coordinates for the highest valued eigenvalues
        sort_indices = np.argsort(evals)[::-1]

        x_v1, y_v1 = evecs[:, sort_indices[0]]
        x_v2, y_v2 = evecs[:, sort_indices[1]]

        # Calculate the center of mass using the moments
        img = cv2.convertScaleAbs(masked_data)
        contours = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cnt = contours[0]
        M = cv2.moments(cnt)
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])

        # Calculate the angle alpha
        alpha = np.arctan(y_v1/x_v1)

        # Calculating the length of the keypoint:
        for i in range(1000000):
            length = i

            x2 = int(cx + length*np.cos(alpha))
            y2 = int(cy + length*np.sin(alpha))

            #if (y2<720 and x2<1080):
            if (y2<256 and x2<256):
                if (img[y2,x2]==0):
                    #print('Line definition complete!')
                    values_reqd = {'cx': cx, 'cy': cy, 'alpha': alpha, 'length': i}
                    break;
            else:
                values_reqd = {'cx': cx, 'cy': cy, 'alpha': alpha, 'length': i}
                break;

        return masked_data, values_reqd, cx, cy, x2, y2
    else:
        return [], {'cx': 0, 'cy': 0, 'alpha': 0, 'length': 0}, 0, 0, 0, 0

def video_maker(imgs, video_name, fps):
    '''
    purpose:
        - Create a video from a set of frames
    
    input:
        - imgs - list of frames (data not file location)
        - video_name - Name of the video
        - fps - Frames per second of the video
    
    output:
        - None
    '''
    import cv2
    frame = cv2.imread(imgs[0])
    height, width, layers = frame.shape
    fourcc = cv2.VideoWriter_fourcc(*'MPEG')
    video = cv2.VideoWriter(video_name, fourcc, fps, (width, height))
    
    for i, image in enumerate(imgs):
        video.write(cv2.imread(image))

    cv2.destroyAllWindows()
    video.release()
    
def get_sec(time):
    import datetime
    '''
    purpose: Convert time from hh:mm:ss format (datetime or string) to seconds
    
    input: 
        - Time in hh:mm:ss format (datetime or string)
        
    output:
        - Time in seconds    
    '''

    if (type(time) is datetime.time):
        h,m,s = time.hour, time.minute, time.second
        return int(h)*3600+int(m)*60+int(s)
    elif (type(time) is datetime.datetime):
        print(time.time)
        print(type(time.time))
        h,m,s = time.time.hour, time.time.minute, time.time.second
        return int(h)*3600+int(m)*60+int(s)
    elif (type(time) is str):
        h,m,s = time.split(':')
        return int(h)*3600+int(m)*60+int(s)
    else:
        print()
        print('Input isn\'t of datatime or string format! FIX BEFORE CONTINUING!')
        print()
    
                           
