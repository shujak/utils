import os
import glob
import utils
import numpy as np
import pandas as pd
from utils import extract_clip
from utils import create_dir
from utils import get_sec

'''
Usage:
    ==> Run this script with the following command:
        python /path_to_script/video_clipper.py

    ==> Make sure that the folder structure is as follows:
        - env
        - utils.py
        - video_clipper.py
        - raw_video
        - raw_video/output
        - CLip Log Template.xlsx
    
    ==> The video filenames should be of the following format:
        - ./{CaseID}_{Feed} (This is extracted directly from the headers in the excel file)

'''

'''
Iniitalize Variables:

    ===> Enter the relative/absolute paths of the folders
'''
excel_loc = './Clip Log Template.xlsx'
vids_input = './raw_videos/'

# Note: Create this folder inside the folder that has all of the inputs
clips_output = './raw_videos/output/'
vids = glob.glob(vids_input+'/*.mp4')

'''
Printing the following videos:

'''
temp = [v for v in vids]
print(temp)

'''
Extract data from the excel file

'''
# Extract data
df = pd.read_excel(excel_loc)
case_ids = list(df['CaseID '])
print('##########################################################')
print('DISCLAIMER: Make sure that the excel is in the proper     ')
print('    format for processing. If it is not consistent,       ')
print('    i.e. the headers aren\'t the same, the program will go')
print('    BOOM (figuratively)                                   ')
print('##########################################################')

# Testing... testing... one... two...
print(df)

# Status update:
print('Total no. of clips to process: {}'.format(len(df)))

'''
Go through each of the videos in the extracted batch and clip them accordingly...

'''
for indx, case in enumerate(case_ids):
    feed = int(list(df['Feed'])[indx])
    init = list(df['Start Time '])[indx]
    fini = list(df['End Time '])[indx]
    init_s = get_sec(init)
    fini_s = get_sec(fini)
    desc = list(df['Description '])[indx].replace(" ", "").replace("-", "_")
    #input_vid = vids_input+str(case)+'_'+str(feed)+'.mp4'    
    input_vid = vids_input+str(case)+'.mp4'
    fn_new = (str(case)+'_'+str(feed)+'_s'+str(init)+'_e'+str(fini)).replace(":", "_")
    output_vid = clips_output+fn_new+'.mp4'
    #fps = os.system("ffprobe -v 0 -of csv=p=0 -select_streams v:0 -show_entries stream=r_frame_rate "+input_vid)
    fps = 30 # Please specify the frame rate
    
    # Extract the clip
    print(init_s)
    print(fini_s)
    print(fps)
    # Adds offset at the beginning
    #os.system("/home/shujakhalid/.imageio/ffmpeg/ffmpeg-linux64-v3.3.1 -y "+" -i "+input_vid+" -ss "+str(init_s)+ " -to "+str(fini_s)+" -c:v copy -c:a copy "+output_vid)
    #os.system("/home/shujakhalid/.imageio/ffmpeg/ffmpeg-linux64-v3.3.1 -y "+" -i "+input_vid+" -ss "+str(init_s)+ " -to "+str(fini_s)+" "+output_vid) # Misses one frame for some reason
    os.system("ffmpeg -ss "+str(init_s)+" -i "+input_vid+" -c:v libx264 -pix_fmt yuv420p -c:a aac -strict -2 -frames:v "+str(abs(init_s-fini_s)*fps)+" "+output_vid)

    
    # Check no. of frames in the file
    #ffmpeg -i filename -vcodec copy -acodec copy -f null /dev/null 2>&1 | grep 'frame=' | cut -f 2 -d ' '

'''
Print disclaimer!

'''
print('##########################################################')
print('DISCLAIMER: Only .mp4 videos will be seen by the program; ')
print('     For any other extension, please modify the code!     ')
print('##########################################################')
