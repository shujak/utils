#!/bin/sh

SOURCE_DIR="./../OSATS_data/Period_3/tool_clips_segmentation/"
DESTINATION_DIR="./../OSATS_data/Period_3/tool_clips_segmentation/TRAINING/Sophia/"
tools=$(ls $SOURCE_DIR)

echo "Splitting files..."

# for element in "${#tools[@]}"
for element in ${tools}
do
	if [ "$element" != "TRAINING" ]
	then
		echo "$element"
	fi
done