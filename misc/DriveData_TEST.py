import glob
import numpy as np
from PIL import Image
# Define the class here
class DriveData():
    __xs = []
    __ys = [] 

    def __init__(self, folder_dataset, state, transform=None):

        self.transform = transform

        if (state=='train'):
            self.labels_file = "./../csv-data/labels_framelevel_train.txt"
        elif (state=='valid'):
            self.labels_file = "./../csv-data/labels_framelevel_valid.txt"
        elif (state=='test'):
            self.labels_file = "./../csv-data/labels_framelevel_test.txt"

        # Assign the paths to each folder here
        # Note: data.txt should have alternating error 
        # and non-error directories
        #print(self.labels_file)
        with open(self.labels_file) as f:

            self.__xs = []
            self.__ys = [] 
            self.__zs = [] 

            for k, line in enumerate(f):
                # Image path ({case}_3_{event})
#                 if (line.split(' , ')[1]=='True'):
#                     file_name = folder_dataset+'OSATS_tool_frames_framelevel/'+line.split(' , ')[0].split('/')[-1]
#                 else:
#                     file_name = folder_dataset+'OSATS_tool_frames_ext_framelevel/'+line.split(' , ')[0].split('/')[-1]
                
                if (state=='test'):
                    file_name = line.split(',')[0]
                    tool_flag = line.split(',')[1]
                else:
                    file_name = line.split(' , ')[0]
                    tool_flag = line.split(' , ')[1]

                if (tool_flag == 'False'):
                    label = '['+ str(1) + ']'
                else:
                    label = '['+ str(0) + ']'

                # add folder names here
                self.__xs.append(file_name)

                # add the actual label data here (not just the paths)
                self.__ys.append(np.squeeze(eval(label)))
                
                # add the filename
                ext = "_"+str(k).zfill(6)+"_pred.jpg"
                self.__zs.append("./results/"+file_name.split('/')[-1].split('.')[0]+ext)
                
#                 # FOR TESTING
#                 if (k==5):
#                     break;

    # Override to give pyTorch access to any image on the dataset
    def __getitem__(self, index):
        import glob
        import numpy as np
        from PIL import Image
        import cv2
        import torch
        import matplotlib.pyplot as plt

        #print(self.__xs)
        # Loop through each folder here and extract the images
        file_name = self.__xs[index]
        label = []        
        resnet = False

        img_array = np.zeros((0,224,224))
            
        img = Image.open(file_name)
        #print('Filename for {} : {}'.format(index, file_name))
#             plt.figure()
#             plt.imshow(img)
#             plt.title(file_name)
        #img = img.convert('L')

        img = cv2.resize(np.asarray(img), (224,224)) # resize image here
        
        #img = cv2.normalize(img, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
        img = cv2.normalize(img, dst=None, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

        img = np.moveaxis(img,-1,0) # resize image here

        img = torch.from_numpy(img).type(torch.FloatTensor)

        # Add the labels here
        label.append(np.squeeze(self.__ys[index]))

        # Convert the image and label to torch tensors
        img_array = torch.from_numpy(np.asarray(img))
        labely = torch.from_numpy(np.asarray(label[0]))
        filenames = self.__zs[index]
        
        return img_array, labely, filenames # Return the set of frames here

    # Override to give PyTorch size of dataset
    def __len__(self):
        return len(self.__xs)        
